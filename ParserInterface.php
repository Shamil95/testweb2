<?php

interface ParserInterface{
	public function process(string $url, strin $tag): array;
}